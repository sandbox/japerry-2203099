function load_home_map() {
	if (GBrowserIsCompatible()) {
		var baseIcon = new GIcon();
		baseIcon.shadow = "http://www.google.com/mapfiles/shadow50.png";
		baseIcon.iconSize = new GSize(20, 34);
		baseIcon.shadowSize = new GSize(37, 34);
		baseIcon.iconAnchor = new GPoint(9, 34);
		baseIcon.infoWindowAnchor = new GPoint(9, 2);
		baseIcon.infoShadowAnchor = new GPoint(18, 25);

		var map = new GMap2(document.getElementById("map"));
		map.addControl(new GLargeMapControl());
		map.addControl(new GMapTypeControl());

		map.setCenter(new GLatLng(home_latitude, home_longitude), 15);

		var bounds = map.getBounds();
		var width = bounds.maxX - bounds.minX;
		var height = bounds.maxY - bounds.minY;
		var point = new GPoint(home_longitude, home_latitude);
		var marker = new GMarker(point);

		var html = "<div style='white-space: nowrap;'>"+ home_address +"<br />"+ home_city;

		if (home_state != '') {
			html += ", "+ home_state;
		}
		if (home_country != '') {
			html += "<br />"+ home_country;
		}

		html += "<br /><b>Your Address:</b><br />";
		html += "<form action=\"http://maps.google.com/maps\" method=\"get\" target=\"_blank\">";
		html += "<input  name=\"saddr\" type=\"text\" SIZE=25 MAXLENGTH=40 value=\"\" />";
		html += "<input type=\"hidden\" name=\"daddr\" id=\"daddr\" value=\""+ home_address +","+ home_city +","+ home_state +","+ home_country +"\"><br />";
		html += "<INPUT value=\"Get Directions\" TYPE=\"SUBMIT\"></form></div>";

		map.addOverlay(marker);
		marker.openInfoWindowHtml(html);
		GEvent.addListener(marker, "click", function() {marker.openInfoWindowHtml(html);});
	}
}

HOME MODULE README
====================================

This module is designed to allow users to post and edit homes.

NOTE: This module is still in a beta/testing state. It is not intended to be used on commercial or production sites.


COMPATABILITY
====================================

1) This module was tested with MySQL. PGSQL is not yet supported.
2) This module requires a goodle key to display the maps.


INSTALLATION
====================================

1) Upload the entire "home" folder into the "modules" directory of your drupal installation.
2) Log in as an administrator of your website.
3) Enable the "home" module on the modules page (http://example.com/?q=admin/build/modules).
   ** Optionally, enable the helper modules:
      a) Home Browse - Provides a browse feature for homes based on country, state, city, and home type.
      b) Home Flyer - Provides users with the ability to print flyers of their own homes.
      c) Home Print - Allows users to print printer-friendly pages of homes.
      etc.
4) Visit http://www.google.com/apis/maps/signup.html to sign up for a google maps key.
5) Apply the key to your website at the "Home Settings" page (http://example.com/?q=admin/settings/home)


FREE FLASH IMAGE VIEWER
====================================

The flash image viewer used is from http://www.airtightinteractive.com/simpleviewer/
If you wish to purchase a copy without the "simpleviewer" link in the lower right hand corner, just replace the "viewer.swf" in the home module directory with the purchased one.


PROBLEMS/SUPPORT
====================================

If there are any problems or issues, please visit http://pointhomes.com


IMPROVEMENTS
====================================

Please provide any improvements, bug fixes, and suggestions at http://pointhomes.com



PHP OPTIMIZATIONS
====================================

Because of the image handling capabilities of the module, it may require modifications to the php.ini file. You may need to modify the variables: memory_limit and upload_max_filesize.


THEMING
====================================

You can override the theme functions by overriding the theme_ function in a template.php file placed in your theme directory. See http://drupal.org/node/11811 for more details.

There are currently these function that can be themed:
  - theme_home_output: Styles and orders the home fields (q=node/x).
  - theme_home_print: The print layout of the printer friendly version of the page (q=node/x/print).
  - theme_home_gallery_edit_image: The layout of the images in the gallery editing page (q=node/x/edit_gallery).

As an example, to theme the "theme_home_output" function, you would need a file named "template.php" in the directory of your theme. Then add the function phptemplate_home_output.

